# Documentación

## Name
Nombre del proyecto Inditex_Prueba

## Description
En la base de datos de comercio electrónico de la compañía disponemos de la tabla PRICES que refleja el precio final (pvp) y la tarifa que aplica a un producto de una cadena entre unas fechas determinadas. A continuación se muestra un ejemplo de la tabla con los campos relevantes:

```
PRICES
-------
 
BRAND_ID         START_DATE                                    END_DATE                        PRICE_LIST                   PRODUCT_ID  PRIORITY                 PRICE           CURR
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1         2020-06-14-00.00.00                        2020-12-31-23.59.59                        1                        35455                0                        35.50            EUR
1         2020-06-14-15.00.00                        2020-06-14-18.30.00                        2                        35455                1                        25.45            EUR
1         2020-06-15-00.00.00                        2020-06-15-11.00.00                        3                        35455                1                        30.50            EUR
1         2020-06-15-16.00.00                        2020-12-31-23.59.59                        4                        35455                1                        38.95            EUR

```
Campos: 
 
* BRAND_ID: foreign key de la cadena del grupo (1 = ZARA).
* START_DATE , END_DATE: rango de fechas en el que aplica el precio tarifa indicado.
* PRICE_LIST: Identificador de la tarifa de precios aplicable.
* PRODUCT_ID: Identificador código de producto.
* PRIORITY: Desambiguador de aplicación de precios. Si dos tarifas coinciden en un rago de fechas se aplica la de mayor prioridad (mayor valor numérico).
* PRICE: precio final de venta.
* CURR: iso de la moneda.
 
Se pide:
 
Construir una aplicación/servicio en SpringBoot que provea una end point rest de consulta  tal que:
 
Acepte como parámetros de entrada: fecha de aplicación, identificador de producto, identificador de cadena.
Devuelva como datos de salida: identificador de producto, identificador de cadena, tarifa a aplicar, fechas de aplicación y precio final a aplicar.
 
Se debe utilizar una base de datos en memoria (tipo h2) e inicializar con los datos del ejemplo, (se pueden cambiar el nombre de los campos y añadir otros nuevos si se quiere, elegir el tipo de dato que se considere adecuado para los mismos).

## Build
El microservicio se ha contruido usando Java la versión 11 en el ide Eclipse con el framework Spring Boot con la base de datos H2 (base de datos en memoria) con JPA para la conexión y mapeo de las clases, se ha documentado la estructura la estructura del microservicio con Swagger 3, y por último se ha realizado prueba con Junit y mockito. 

## Installation
Una vez que se arranque el microservicio, ya sea por linea de comando (Usando maven) o bien desde un framework tendremos las siguientes URLs activas:
- [ ] [H2](http://localhost:8080/h2-console) para conectarse a la base de datos.
- [ ] [Swagger](http://localhost:8080/swagger-ui/index.html) para ver la documentación de la estructura del microservicio y para realizar pruebas.

Para probar con Postman o algún programa similar un ejemplo de petición de entrada sería:

POST [URL](http://localhost:8080/price)

```
{
  "fecha": "2020-06-14T16:00:00.000Z",
  "product_id": 35455,
  "brand_id": 1
}
```

## Author

### Realizada por Jesús Alejandro Benítez Pedrero
