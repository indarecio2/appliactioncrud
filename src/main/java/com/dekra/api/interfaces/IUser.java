package com.dekra.api.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.dekra.api.model.User;

@Repository
public interface IUser extends CrudRepository<User, Integer> {

}
