package com.dekra.api.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dekra.api.interfaces.IUser;
import com.dekra.api.interfacesService.IUserSerrvice;
import com.dekra.api.model.User;

@Service
public class UserService implements IUserSerrvice {
	
	@Autowired
	private IUser data;

	@Override
	public List<User> listas() {
		return (List<User>)data.findAll();
	}

	@Override
	public Optional<User> listarId(int id) {
		return data.findById(id);
	}

	@Override
	public int guardar(User user) {
		int res=0;
		User u=data.save(user);
		if(!u.equals(null)) {
			res=1;
		}
		return res;
	}

	@Override
	public void eliminar(int id) {
		data.deleteById(id);
	}

}
