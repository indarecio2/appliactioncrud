package com.dekra.api.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String username;
	private String name;
	private String surnames;
	private String email;
	private String password;
	private String age;
	private String active;
	private Date  lastLogging;
	private Date creationDate;
	
	public User() {
		// TODO Auto-generated constructor stub
	}
	
	public User(int id, String username, String name, String surnames, String email, String password, String age,
			String active, Date lastLogging, Date creationDate) {
		super();
		this.id = id;
		this.username = username;
		this.name = name;
		this.surnames = surnames;
		this.email = email;
		this.password = password;
		this.age = age;
		this.active = active;
		this.lastLogging = lastLogging;
		this.creationDate = creationDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurnames() {
		return surnames;
	}
	public void setSurnames(String surnames) {
		this.surnames = surnames;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public Date getLastLogging() {
		return lastLogging;
	}
	public void setLastLogging(Date lastLogging) {
		this.lastLogging = lastLogging;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	

}
