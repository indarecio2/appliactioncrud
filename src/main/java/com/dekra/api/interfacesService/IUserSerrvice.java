package com.dekra.api.interfacesService;

import java.util.List;
import java.util.Optional;

import com.dekra.api.model.User;

public interface IUserSerrvice {
	public List<User> listas();
	public Optional<User>listarId(int id);
	public int guardar(User user);
	public void eliminar(int id);
}
