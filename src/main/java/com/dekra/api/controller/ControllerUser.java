package com.dekra.api.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dekra.api.interfacesService.IUserSerrvice;
import com.dekra.api.model.User;

@Controller
@RequestMapping
public class ControllerUser {
	
	@Autowired
	private IUserSerrvice service;
	
	@GetMapping("/list")
	public String list(Model model) {
		List<User>users=service.listas();
		model.addAttribute("users", users);
		return "index";
	}
	
	@GetMapping("/new")
	public String add(Model model) {
		model.addAttribute("user", new User());
		return "form";
	}
	
	@PostMapping("/save")
	public String save(@Validated User user, Model model) {
		service.guardar(user);
		return "redirect:/list";
		
	}
	
	@GetMapping("/modify/{id}")
	public String modify(@PathVariable int id, Model model) {
		Optional<User>user=service.listarId(id);
		model.addAttribute("user", user);
		return "form";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(Model model, @PathVariable int id) {
		service.eliminar(id);
		return "redirect:/list";
	}

}
